function curved_line_generate(LatStart, LngStart, LatEnd, LngEnd, Color, Horizontal, Multiplier) {

    var LastLat = LatStart;
    var LastLng = LngStart;

    var PartLat;
    var PartLng;

    var Points = new Array(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9);
    var PointsOffset = new Array(0.2, 0.35, 0.5, 0.55, 0.60, 0.55, 0.5, 0.35, 0.2);

    var OffsetMultiplier = 0;

    if (Horizontal == 1) {
        var OffsetLenght = (LngEnd - LngStart) * 0.1;
    } else {
        var OffsetLenght = (LatEnd - LatStart) * 0.1;
    }

    for (var i = 0; i < Points.length; i++) {
        if (i == 4) {
            OffsetMultiplier = 1.5 * Multiplier;
        }

        if (i >= 5) {
            OffsetMultiplier = (OffsetLenght * PointsOffset[i]) * Multiplier;
        } else {
            OffsetMultiplier = (OffsetLenght * PointsOffset[i]) * Multiplier;
        }

        if (Horizontal == 1) {
            PartLat = (LatStart + ((LatEnd - LatStart) * Points[i])) + OffsetMultiplier;
            PartLng = (LngStart + ((LngEnd - LngStart) * Points[i]));
        } else {
            PartLat = (LatStart + ((LatEnd - LatStart) * Points[i]));
            PartLng = (LngStart + ((LngEnd - LngStart) * Points[i])) + OffsetMultiplier;
        }

        curved_line_create_segment(LastLat, LastLng, PartLat, PartLng, Color);

        LastLat = PartLat;
        LastLng = PartLng;
    }

    curved_line_create_segment(LastLat, LastLng, LatEnd, LngEnd, Color);

}

function curved_line_create_segment(LatStart, LngStart, LatEnd, LngEnd, Color) {
    var LineCordinates = new Array();

    LineCordinates[0] = new google.maps.LatLng(LatStart, LngStart);
    LineCordinates[1] = new google.maps.LatLng(LatEnd, LngEnd);

    var Line = new google.maps.Polyline({
        path: LineCordinates,
        geodesic: false,
        strokeColor: Color,
        strokeOpacity: 1,
        strokeWeight: 3
    });

    Line.setMap(map);
}


function initialize() {

    /* Create Google Map */

    var styles = [
        {
            stylers: [
                { hue: "#000000" },
                { saturation: -100 }
            ]
        },{
            featureType: "road",
            elementType: "geometry",
            stylers: [
                { lightness: 100 },
                { visibility: "simplified" }
            ]
        },{
            featureType: "road",
            elementType: "labels",
            stylers: [
                { visibility: "off" }
            ]
        }
    ];

    var myOptions = {
        zoom: 3,
        scrollwheel: false,
        center: new google.maps.LatLng(29.372351, 2.773125),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: styles
    };

    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

    /* Add Sample Lines */

    /* Los angeles - Chicago */
    curved_line_generate(34.058621, -118.242818, 41.880017, -87.634652,"#000000", false, 4);

    /* Chicago - New York */
    curved_line_generate(41.880017, -87.634652, 40.713463, -74.007147,"#000000", false, 4);

    /* New York - Toronto */
    curved_line_generate( 40.713463, -74.007147 , 43.655927, -79.378745, "#000000", false, 4);

    /* New York - London */
    curved_line_generate( 40.713463, -74.007147 , 51.509201, -0.127005, "#000000", false, 4);

    /* London - Paris */
    curved_line_generate(51.509201, -0.127005, 48.857221, 2.350303,  "#000000", false, 4);

    /* Paris - Marseille */
    curved_line_generate(48.857221, 2.350303, 43.295635, 5.361880,  "#000000", false, 4);

    /* Marseille - Jeddah */
    curved_line_generate(43.295635, 5.361880, 21.293000, 39.241495,  "#000000", false, 4);


    /* Jeddah - Singapore */
    curved_line_generate(21.293000, 39.241495, 1.279378, 103.839460,  "#000000", false, 4);


    /* Jeddah - Riyadh */
    curved_line_generate(21.293000, 39.241495, 24.760737, 46.882954,  "#999999", false, 4);

    /* Riyadh - Ghunan */
    curved_line_generate(24.760737, 46.882954, 26.133352, 49.899442,  "#999999", false, 4);

    /* Ghunan -  Al Fahdhili*/
    curved_line_generate(26.133352, 49.899442, 26.403641, 50.120486,  "#999999", false, 4);

    /* Al Fahdhili - Kuwait */
    curved_line_generate(26.403641, 50.120486, 29.371404, 47.968335,  "#999999", false, 4);


    /* Ghunan -   Bahrain */
    curved_line_generate(26.133352, 49.899442, 26.242507, 50.576779,  "#999999", false, 4);


    /* Ghunan -   Salwa */
    curved_line_generate(26.133352, 49.899442, 24.741333, 50.756192,  "#999999", false, 4);

    /* Salwa - Qatar */
    curved_line_generate(24.741333, 50.756192, 25.358234, 51.184398, "#999999", false, 4);


    /* Salwa - UAE */
    curved_line_generate(24.741333, 50.756192, 24.304627, 54.593008, "#999999", false, 4);

    /* UAE - Fujairah */
    curved_line_generate(24.304627, 54.593008, 25.399358, 56.231446,  "#999999", false, 4);

    /* Fujairah - Mazara */
    curved_line_generate(25.399358, 56.231446, 37.666585, 12.561583, "#2ae50c", false, 4);

    /* Mazara - Frankfurt */
    curved_line_generate(37.666585, 12.561583, 50.116190, 8.684776, "#2ae50c", false, 4);

    /* Frankfurt - Amsterdam */
    curved_line_generate(50.116190, 8.684776, 52.375934, 4.889356, "#2ae50c", false, 4);

    /* Amsterdam - London */
    curved_line_generate(52.375934, 4.889356, 51.509201, -0.127005, "#2ae50c", false, 4);


    /* London - Toranto */
    curved_line_generate(51.509201, -0.127005, 43.655927, -79.378745,  "#2ae50c", false, 4);

    /* Toranto */
    curved_line_generate(43.655927, -79.378745,  41.880017, -87.634652,  "#2ae50c", false, 4);










    var locations = [
        ['Los Angeles', 34.058621, -118.242818,'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Chicago', 41.880017, -87.634652,'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['New York', 40.713463, -74.007147,'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Toronto', 43.655927, -79.378745,'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['London', 51.509201, -0.127005,'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Paris', 48.857221, 2.350303, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Marseille', 43.295635, 5.361880, 'http://labs.google.com/ridefinder/images/mm_20_black.png'],
        ['Jeddah', 21.293000, 39.241495, 'http://labs.google.com/ridefinder/images/mm_20_black.png'],
        ['Singapore', 1.279378, 103.839460, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Riyadh', 24.760737, 46.882954,'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Ghunan', 26.133352, 49.899442,'http://labs.google.com/ridefinder/images/mm_20_red.png' ],
        ['Al Fadhhili', 26.403641, 50.120486, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Kuwait', 29.371404, 47.968335, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Bahrain', 26.242507, 50.576779, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Salwa', 24.741333, 50.756192, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Qatar', 25.358234, 51.184398, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['UAE', 24.304627, 54.593008, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Fujairah', 25.399358, 56.231446, 'http://labs.google.com/ridefinder/images/mm_20_black.png'],
        ['Mazara', 37.666585, 12.561583 , 'http://labs.google.com/ridefinder/images/mm_20_black.png'],
        ['Frankfurt', 50.116190, 8.684776, 'http://labs.google.com/ridefinder/images/mm_20_red.png'],
        ['Amsterdam', 52.375934, 4.889356, 'http://labs.google.com/ridefinder/images/mm_20_red.png']
    ];




    for (var i = 0; i < locations.length; i++) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            labelContent: locations[i][0],
            labelAnchor: new google.maps.Point(locations[i][1], locations[i][2]),
            labelClass: "map-labels", // the CSS class for the label
            icon: locations[i][3]
        });
    }

}