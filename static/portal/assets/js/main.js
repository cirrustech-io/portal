/**
 * Created by sunnizar on 07/03/15.
 */


/*
 * A self invoking anonymous function to project $ namespace.
 * Also the function executes when the
 * */

var TwoConnect = {};

(function($){


    $(function(){

        TwoConnect.init();

    });


    TwoConnect = TwoConnect || {};

    TwoConnect = {

        init: function () {
            $('.svg').inlineSVG();
            $(".has-underlay").underlay();

            this.initCarousels();
            this.initSiteSearch();

            /*this.animateElements();*/

            this.mobileMenu();
            this.accordion();
            this.generateEmbededContent();

            this.smoothInternalLink();

            this.tabControl();
            this.accordionControl();

            if($(".year-button-wrapper").length) {
                this.stickyHeader($(".year-button-wrapper"));
            }

            if($(".year-button-wrapper").length) {
                this.timelineNavigation();
            }


            //If graph required and usage container  available then init graph
            if($("#usage").length) {
                this.initChart();
            }

            //If datapicker available then init calender values
            if($(".datepicker").length) {
                this.configCalenderValues();
            }






        },
        unique: function(list) {
            var result = [];
            $.each(list, function(i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        },
        elementInViewport: function(el) {
            var top = el.offsetTop;
            var left = el.offsetLeft;
            var width = el.offsetWidth;
            var height = el.offsetHeight;

            while(el.offsetParent) {
                el = el.offsetParent;
                top += el.offsetTop;
                left += el.offsetLeft;
            }

            return (
            top < (window.pageYOffset + window.innerHeight) &&
            left < (window.pageXOffset + window.innerWidth) &&
            (top + height) > window.pageYOffset &&
            (left + width) > window.pageXOffset
            );
        },
        timelineScroll: function(){
            var aChildren = $(".year-button-view .years li").children(); // find the a children of the list items
            var aArray = []; // create the empty aArray
            for (var i=0; i < aChildren.length; i++) {
                var aChild = aChildren[i];
                var ahref = $(aChild).attr('href');
                aArray.push(ahref);
            } // this for loop fills the aArray with attribute href values

            $(window).scroll(function(){
                var windowPos = $(window).scrollTop() + 120; // get the offset of the window from the top of page
                var windowHeight = $(window).height(); // get the height of the window
                var docHeight = $(document).height();

                for (var i=0; i < aArray.length; i++) {
                    var theID = aArray[i];
                    var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                    var divHeight = $(theID).height() + 50; // get the height of the div in question
                    if (windowPos >= divPos && windowPos < (divPos + divHeight)) {

                        $("ul.years li").removeClass("active");

                        var $element = $("a[href='" + theID + "']");
                        $element.parent().addClass("active");


                        if(!$element.parent().isOnScreen() && $element.parent().offset().left > 0) {
                            $element.closest(".year-button-view").animate({scrollLeft: $element.closest(".year-button-view").scrollLeft()  +  $element.parent().outerWidth() + 50}, 400);
                        }

                        if(!$element.parent().isOnScreen() && $element.parent().offset().left < 0) {
                            $element.closest(".year-button-view").animate({scrollLeft: $element.parent().offset().left + 100}, 400);
                        }


                    } else {
                        $("a[href='" + theID + "']").parent().removeClass("active");
                    }
                }

                if(windowPos + windowHeight == docHeight) {
                    if (!$("nav li:last-child a").parent().hasClass("active")) {
                        var navActiveCurrent = $(".active a").attr("href");
                        $("a[href='" + navActiveCurrent + "']").parent().removeClass("active");
                        $("nav li:last-child a").parent().addClass("active");
                        //console.log($("nav li:last-child a").parents("year-button-view").scrollLeft());
                    }
                }
            });


        },
        timelineNavigation: function(){
            var yearsContainer = $('[class="year"]');
            var yearsArray = [];
            var yearNav = "";

            yearsContainer.each(function(index){
                yearsArray[index] = $(this).html();
            });
            yearsArray = this.unique(yearsArray);


            $.each(yearsArray, function( index, value ) {
                yearNav += '<li id="nav-year-'+ value +'"><a href="#year-'+value+'">'+ value +'</a></li>';
            });

            $(".year-button-view .years").html(yearNav);
            this.smoothInternalLink();
            this.timelineScroll();

        },
        stickyHeader:function($ele){
            var pos = $ele.offset(),
                topMargin = 0;

            if ($("header.header").css("position") === "fixed") {
                topMargin = 50;
            }

            $(window).scroll(function() {
                if ($(this).scrollTop() > pos.top - topMargin){
                    $ele.addClass("sticky");
                } else {
                    $ele.removeClass("sticky");
                }

            });

        },
        horizontalScroll:function(scrollable,target){
            $(scrollable).animate({scrollLeft: $(target).position().left}, 500);
        },
        accordionControl:function(){
            var $accordionButton = $(".accordion-wrapper h2"),
                $firstAccordionButton = $(".accordion-wrapper:first-child h2");

            $accordionButton.removeClass("active");
            $firstAccordionButton.addClass("active");

            $accordionButton.on("click", function(){
                $accordionButton.removeClass("active");
                $(this).addClass("active");
            });


        },
        tabControl:function(){
            var $tabButton = $("ul.tabs li"),
                $firstTabButton = $("ul.tabs li:first-child"),
                $firstTabContent = $firstTabButton.find("a").attr("href"),
                $tabContent = $(".tab-content-wrapper");

            $firstTabButton.addClass("active");
            $($firstTabContent).addClass("active");

            $tabButton.on("click", function(){
                $tabContent.removeClass("active");
                $tabButton.removeClass("active");
                $(this).addClass("active");
                $($(this).find("a").attr("href")).addClass("active");
                TwoConnect.accordionControl();
            });


            //Tab control based on url #value
            // open content that matches the hash
            var hash = window.location.hash;
            var thash = hash.substring(hash.lastIndexOf('#'), hash.length);
            $('.tabs').find('a[href*='+ thash + ']').trigger('click');


        },
        smoothInternalLink: function(){
            $('a[href^="#"]').on('click',function (e) {
                e.preventDefault();

                if($(this).attr("href").indexOf("#tab") == -1) {
                    var target = this.hash,
                        $target = $(target),
                        $scrollMargin = 50;


                    if ($("header.header").css("position") === "fixed") {
                        $scrollMargin = 100;
                    }

                    $('html, body').stop().animate({
                        /*'scrollTop': $target.offset().top - $scrollMargin*/
                    }, 700, 'swing', function () {
                        window.location.hash = target;
                    });
                }
            });
        },
        generateEmbededContent: function(){
            $('.embeded-from-url').html(function(i, html) {
                return html.
                    replace(/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([^\ ]+)/g,
                    '<iframe width="200" height="100" src="http://www.youtube.com/embed/$1?rel=0&amp;controls=1" frameborder="0" allowfullscreen></iframe>').
                    replace(/(?:http:\/\/)?(?:www\.)?(?:vimeo\.com)\/([^\ ]+)/g,
                    '<iframe src="//player.vimeo.com/video/$1" width="200" height="100" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>').
                    replace(/(?:https:\/\/)?(?:www\.)?(?:google\.com\/maps)\/(?:embed\?pb=)?([^\ ]+)/g,
                    '<iframe src="https://www.google.com/maps/embed?pb=$1" width="200" height="100" frameborder="0" allowfullscreen></iframe>');

            });
        },
        accordion: function(){

            $('.accordion-title').on('click', function(){
                var $title = $(this);
                $(this).next('.accordion-content-wrapper').toggleClass('show').promise().done(function(){
                    $title.toggleClass("active");
                    $title.toggleClass("icon-up");
                });
            });
        },
        animateElements: function(){

            //Animate Featured List
            $(".featured-list ul").inViewport(function(px){
                if(px) $(this).addClass("inView") ;
            });
        },
        scrollToPos: function(pos,speed){
            console.log(pos);
            $('html,body').animate({
                scrollTop: pos
            }, speed);
        },
        mobileMenu: function(){

            var Menu = {
                el: {
                    ham: $('header.header .menu-button'),
                    menuTop: $('.menu-top'),
                    menuMiddle: $('.menu-middle'),
                    menuBottom: $('.menu-bottom')
                },
                menuActive: false,
                nav: $("header.header .nav-wrapper"),
                master: $(".master"),
                overlay: $(".page-header .underlay"),
                scrollPosition:0,
                init: function() {
                    Menu.bindUIactions();
                },

                bindUIactions: function() {
                    Menu.el.ham
                        .on(
                        'click',
                        function(event) {
                            if(Menu.menuActive) {
                                Menu.menuActive = false;
                                Menu.nav.removeClass("show");
                                Menu.master.removeClass("inactive");
                                Menu.master.css('top',0);
                                TwoConnect.scrollToPos(Menu.scrollPosition,0);
                                Menu.overlay.hide();

                            } else {
                                Menu.menuActive = true;
                                Menu.nav.addClass("show");
                                Menu.scrollPosition = $(window).scrollTop();
                                console.log(Menu.scrollPosition);
                                Menu.master.css('top',"-"+Menu.scrollPosition+"px");
                                Menu.master.addClass("inactive");
                                Menu.overlay.show();

                            }
                            Menu.activateMenu(event);
                            event.preventDefault();
                        }
                    );

                },

                activateMenu: function() {
                    Menu.el.menuTop.toggleClass('menu-top-click');
                    Menu.el.menuMiddle.toggleClass('menu-middle-click');
                    Menu.el.menuBottom.toggleClass('menu-bottom-click');
                }
            };

            Menu.init();

        },
        initSiteSearch: function(){
            var $searchForm = $("li.search form"),
                $searchLabel = $("li.search form #search-label"),
                $searchInput = $("li.search form .input input");

            $searchLabel.on('click', function(){
                $searchForm.addClass("active");
            });

            $searchInput.on('blur', function(){
                $searchForm.removeClass("active");
            });


        },
        initCarousels: function(){

            $owlContainer = $('.owl-carousel');
            $owlSlides    = $owlContainer.children('.item');

            if ($owlSlides.length > 1) {
                $owlContainer.owlCarousel({
                    autoPlay : 7000,
                    stopOnHover : true,
                    navigation:true,
                    paginationSpeed : 1000,
                    goToFirstSpeed : 2000,
                    singleItem : true,
                    autoHeight : true,
                    navigationText: false

                });

            } else {
                $owlContainer.addClass("no-carousel");

            }
        },
        initChart: function(){

            var ctx = $("#usage").get(0).getContext("2d");
            var myNewChart = new Chart(ctx);

            new Chart(ctx).Line(graphValue,{
                bezierCurve: false,
                scaleIntegersOnly: false,
                scaleOverride: true,
                showTooltips: false,
                responsive: false,
                scaleFontFamily: "'PFDinTextUniversal-Medium'",
                scaleFontSize: 15,
                scaleFontColor: "#56565a",
                scaleShowHorizontalLines: true,
                scaleShowGridLines : false,
                pointDot : false,
                datasetStroke : false,
                scaleSteps: 5,
                scaleStepWidth: .5,
                scaleStartValue: 0.5,
                scaleLabel: "<%=value%> GB"
            });
        },
        configCalenderValues: function(){
            /*for datepicker*/
            var picker = new Pikaday({
                field: document.getElementById('from'),
                format: 'MMM D',
                i18n: {
                    previousMonth : 'Previous Month',
                    nextMonth     : 'Next Month',
                    months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
                    weekdaysShort : ['S','M','T','W','T','F','S']
                }
            });
            var picker = new Pikaday({
                field: document.getElementById('to'),
                format: 'MMM D',
                i18n: {
                    previousMonth : 'Previous Month',
                    nextMonth     : 'Next Month',
                    months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
                    weekdaysShort : ['S','M','T','W','T','F','S']
                }
            });
        }

    }


}(jQuery));


/*********************************************************custom js*************************************************/                                         
/*******************************************************************************************************************/

$(function() {

    /***************Account Details page***************/
    // account-table
    $(".account-table a").click(function(){
        var span = '<span> </span>'
        if( $(this).find('span').attr('class') == 'icon-pencil41' ){
            $(this).html(span + 'Save');
            $(this).closest('.row_data').find('input').prop('disabled', false);
            $(this).closest('.row_data').find('input').focus();
            $(this).closest('.row_data').find('a span').attr('class', 'icon-check-box');
        }
        else {
            $(this).html(span + 'Edit');
            $(this).closest('.row_data').find('input').prop('disabled', true);
            $(this).closest('.row_data').find('a span').attr('class', 'icon-pencil41');
        }
    });

    $(".account-table input").focus(function(){
        
    });

    $(".account-table input").blur(function(){
        /*$(this).closest('.row_data').find('a').html('Edit');
        $(this).prop('disabled', true);
        $(this).closest('tr').find('.edit span').attr('class', 'icon-edit-write');*/
    });

    

    /***************billing page***************/

    $('.row_data .chkbox').click(function() {
        var className = $(this).find('.chkbox_icon').attr('class');

        if (className=='chkbox_icon icon-square93') {
            $(this).find('.chkbox_icon').addClass('icon-check-box');
            $(this).find('.chkbox_icon').removeClass('icon-square93');
            $(this).find('input[type="checkbox"]').attr('checked', true);
                        
        }
        else{
            $(this).find('.chkbox_icon').removeClass('icon-check-box');
            $(this).find('.chkbox_icon').addClass('icon-square93');
            $(this).find('input[type="checkbox"]').attr('checked', false);          
        }
        
    });

    $('.bill_table .row_data').hover(function() {
        $(this).find('.chkbox').addClass('active');
        $(this).find('.bil_amt').addClass('active');
        $(this).find('.view_detail').addClass('active');
    }, function() {
        $(this).find('.chkbox').removeClass('active');
        $(this).find('.bil_amt').removeClass('active');
        $(this).find('.view_detail').removeClass('active');
    });

    

    $('.featured-list li').hover(function() {
        $(this).find('.icon').addClass('active');
    }, function() {
        $(this).find('.icon').removeClass('active');
    });


    /***************contacts page table hover***************/

    $('.contacts_table .row_data').hover(function() {
        $(this).find('.edit a').addClass('active');
        $(this).find('.delete a').addClass('active');        
    }, function() {
        $(this).find('.edit a').removeClass('active');
        $(this).find('.delete a').removeClass('active');
    });


    /***************Popup script for contacts page and billing page***************/

    $('a.lightbox-link').magnificPopup({
        type: 'inline',
        midClick: true
    });

    $('.submit_link.contacts a').magnificPopup({
                                                type: 'inline',
                                                midClick: true
                                            });


    /***************Service Details page***************/

    /*radio button function*/

    $('.select_service .option').click(function() {
        $('.select_service').find('span').attr('class', 'icon-circle6');
        $('.select_service').find('input[type="radio"]').attr('checked', false);

        $(this).find('span').removeClass('icon-circle6');
        $(this).find('span').addClass('icon-radio51');            
        $(this).find('input[type="radio"]').attr('checked', true);
    });


    /***************Usage Summary page***************/

    $('.graph_option .select_option').click(function(){
        if($(this).find('.chkbox_icon').attr('class') =='chkbox_icon icon-square93'){
            $(this).find('.chkbox_icon').removeClass('icon-square93').addClass('icon-check-box');
            $(this).find('input[type="checkbox"]').attr('checked', 'checked');
        }else{
            $(this).find('.chkbox_icon').removeClass('icon-check-box').addClass('icon-square93');
            $(this).find('input[type="checkbox"]').attr('checked', false);
        }
    });

    /***************Request New Service page***************/

    /*Requested Service radio button*/
    $('.select_requested_service .option').click(function() {
        $('.select_requested_service').find('span').attr('class', 'icon-circle6');
        $('.select_requested_service').find('input[type="radio"]').attr('checked', false);

        $(this).find('span').removeClass('icon-circle6');
        $(this).find('span').addClass('icon-radio51');            
        $(this).find('input[type="radio"]').attr('checked', true);
    });

    /*Type of internet Service 1*/
    $('#internet_service1 .option').click(function() {
        $('#internet_service1').find('span').attr('class', 'icon-circle6');
        $('#internet_service1').find('input[type="radio"]').attr('checked', false);

        $(this).find('span').removeClass('icon-circle6');
        $(this).find('span').addClass('icon-radio51');            
        $(this).find('input[type="radio"]').attr('checked', true);
    });

    /*Type of internet Service 2*/
    $('#internet_service2 .option').click(function() {
        $('#internet_service2').find('span').attr('class', 'icon-circle6');
        $('#internet_service2').find('input[type="radio"]').attr('checked', false);

        $(this).find('span').removeClass('icon-circle6');
        $(this).find('span').addClass('icon-radio51');            
        $(this).find('input[type="radio"]').attr('checked', true);
    });

    
});