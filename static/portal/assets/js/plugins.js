// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


/* This plugin replaces image svg with inline svg */
(function($){
    $.fn.inlineSVG = function(){

        return this.each(function(){

            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');


        });
    };
})(jQuery);




/* This plugin adds a partially transparent layer under the elements it is invoked for */
(function($){
    $.fn.underlay = function(){
        return this.each(function(){
            $(this).prepend('<div class="underlay"></div>');
        });
    };
})(jQuery);



/* This plugin shows width of the element it is invoked for */
(function($){
    $.fn.showWidth = function(options){
        var settings = $.extend({ position:'TL'},options);
        return this.each(function(){

            $(this).css({position:'relative'}).append('<div class="show-width"></div>');
            var showWidth = $(this).find('.show-width');

            switch(settings.position) {
                case 'TL':
                    showWidth.css({'top':0,'left':0});
                    break;
                case 'TR':
                    showWidth.css({'top':0,'right':0});
                    break;
                case 'BL':
                    showWidth.css({'bottom':0,'left':0});
                    break;
                case 'BR':
                    showWidth.css({'bottom':0,'right':0});
                    break;
                default:
                    showWidth.css({'top':0,'left':0});
            }

            showWidth.css({position:'absolute'}).html('w: '+$(this).innerWidth()+'px, h: '+$(this).innerHeight()+'px');
            $(window).on('resize',function(){
                showWidth.html('w: '+$(this).innerWidth()+'px, h: '+$(this).innerHeight()+'px');
            });
        });
    };
})(jQuery);





/* This plugin will transform a list into a custom drop-down when on mobile / small tablet device*/
(function($){

    $.fn.customDropNav = function(){

        return this.each(function(){

            var tabTmpl = '<div class="drop-tabs icon-dropdown"><div class="drop-wrapper">&nbsp;</div></div>';
            $(this).prepend(tabTmpl);

            var $select = $(this).find('.drop-tabs');
            var $option = $select.next().find('>*');
            var $optionActive = $select.next().find('>.active > a');

            $select.find('.drop-wrapper').html($optionActive.html());

            $select.on('click',function(e){

                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).next().css({'display':'none'});
                }
                else {
                    $(this).addClass('active');
                    $(this).next().css({'display':'block'});
                }
                return false;

            });

            $option.on('click',function(e){
                if(parseInt($('body').width())<737){
                    $select.find('.drop-wrapper').html($(this).find('a').html());
                    $select.removeClass('active');
                    $(this).parent().css({'display':'none'});
                }
            });

            $(window).on('resize',function(e){
                if(parseInt($('body').width())>=737){
                    $option.parent().css({'display':'block'});
                }
            });

        });


    };






})(jQuery);




// Plugin @RokoCB :: Return the visible amount of px
// of any element currently in viewport.
(function($, win) {
    $.fn.inViewport = function(cb) {
        return this.each(function(i,el){
            function visPx(){
                var H = $(this).height(),
                    r = el.getBoundingClientRect(), t=r.top, b=r.bottom;
                return cb.call(el, Math.max(0, t>0? H-t : (b<H?b:H)));
            } visPx();
            $(win).on("resize scroll", visPx);
        });
    };
}(jQuery, window));




//Check if element is offscreen
(function($){
    $.fn.isOnScreen = function(){

        var win = $(window);

        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width() - 50;
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    };
})(jQuery);


