# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-17 12:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0002_auto_20160316_1118'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentdetails',
            name='txn_Ids',
            field=models.TextField(blank=True, null=True),
        ),
    ]
