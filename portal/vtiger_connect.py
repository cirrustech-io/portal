import sys
#sys.path.append('Vtiger/')

from WSClient import *
import settings

"""
client = Vtiger_WSClient('https://intranet.2connectbahrain.com/vtiger/webservice.php')
user_name = 'admin'
user_token = "uCwYQhfa52blx98Y"
"""

#Vtiger Credentials
crm_webservice_url = settings.CRM_URL
crm_user_name = settings.CRM_USER_NAME
crm_user_token = settings.CRM_USER_TOKEN
client = Vtiger_WSClient(crm_webservice_url)

def get_account(crm_id):

  try:
    login = get_login()

    ret_dct = {}
    if not login:
        ret_dct = {"error":"Login failed!"}
    else:
        record = crm_id
        recordInfo = get_retrieve(record)
        if recordInfo:
            ret_dct['account'] = recordInfo
            query = "SELECT * From SalesOrder WHERE account_id='%s'" % (recordInfo['id'])
	    print query
            ret_dct['sales_orders'] = get_query(query)
            #return json.dumps(recordInfo, sort_keys=True, indent=4, separators=(',', ': '))
        else:
            ret_dct = {"error":"Your Account doesn't exists. Try with another account"}
  except Exception,e:
      print e
      ret_dct = {"error":"An error in login. Please try again"}
  return ret_dct

def get_all_accounts(start):

  try:
    login = get_login()

    ret_dct = {}
    if not login:
        ret_dct = {"error":"Login failed!"}
    else:
        query = "SELECT * From Accounts LIMIT %s, 100;" % (str(start))
        ret_dct['accounts'] = get_query(query)
  except Exception,e:
      print e
      ret_dct = {"error":"An error in login. Please try again"}
  return ret_dct

def get_all_contact(crm_id):

  try:
    login = get_login()

    ret_dct = {}
    if not login:
        ret_dct = {"error":"Login failed!"}
    else:
        query = "SELECT * From Contacts WHERE account_id='%s'" % (crm_id)
        ret_dct = get_query(query)
        #return json.dumps(recordInfo, sort_keys=True, indent=4, separators=(',', ': '))
  except Exception,e:
      print e
      ret_dct = {"error":"An error in login. Please try again"}
  return ret_dct
    
def get_login():
    return client.doLogin(crm_user_name, crm_user_token)

def get_retrieve(record):
    get_login()
    return client.doRetrieve(record)

def get_query(query):
    get_login()
    return client.doQuery(query)

def get_describe(module):
    get_login()
    return client.doDescribe(module)

def get_create(module, record):
    get_login()
    return client.doCreate(module, record)

def get_invoke(method, record):
    get_login()
    return client.doInvoke(method, record)

def get_update(module, record):
    get_login()
    return client.doUpdate(module, record)
