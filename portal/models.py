from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    vtiger = models.CharField(max_length=50, null=True, blank=True)

    def __unicode__(self):
        return unicode(self.user)

class PaymentDetails(models.Model):
    user = models.ForeignKey(User, db_index=True)
    account_Id = models.CharField(max_length=50, db_index=True)
    vpc_OrderInfo = models.CharField(max_length=100, db_index=True)
    vpc_MerchTxnRef = models.CharField(max_length=100, db_index=True)
    vpc_Amount = models.FloatField(default=0.0, db_index=True)
    vpc_SecureHash = models.CharField(max_length=200, db_index=True)
    is_hashValidated = models.BooleanField(default=False, db_index=True)
    payment_Status = models.CharField(max_length=100, db_index=True)
    vpc_BatchNo = models.CharField(max_length=100, null=True, blank=True)
    vpc_Message = models.CharField(max_length=100, db_index=True, null=True, blank=True)
    vpc_Card = models.CharField(max_length=100, null=True, blank=True)
    vpc_ReceiptNo = models.CharField(max_length=100, db_index=True, null=True, blank=True)
    vpc_AuthorizeId = models.CharField(max_length=100, null=True, blank=True)
    vpc_TransactionNo = models.CharField(max_length=100, db_index=True, null=True, blank=True)
    vpc_AcqResponseCode = models.CharField(max_length=50, db_index=True, null=True, blank=True)
    vpc_TxnResponseCode = models.CharField(max_length=50, db_index=True, null=True, blank=True)
    vpc_TxnResponseDesc = models.CharField(max_length=200, db_index=True, null=True, blank=True)
    vpc_VerStatus = models.CharField(max_length=100, db_index=True, null=True, blank=True)
    vpc_VerStatusDesc = models.CharField(max_length=200, db_index=True, null=True, blank=True)
    vpc_VerType = models.CharField(max_length=100, null=True, blank=True)
    vpc_VerToken = models.CharField(max_length=100, db_index=True, null=True, blank=True)
    vpc_VerSecurityLevel = models.CharField(max_length=100, db_index=True, null=True)
    vpc_3DSenrolled = models.CharField(max_length=100, null=True, blank=True)
    vpc_3DSXID = models.CharField(max_length=100, null=True, blank=True)
    vpc_3DSECI = models.CharField(max_length=100, null=True, blank=True)
    vpc_3DSstatus = models.CharField(max_length=100, db_index=True, null=True, blank=True)
    is_PGError = models.BooleanField(default=False, db_index=True)
    txn_Ids = models.TextField(null=True, blank=True)
    created_Date = models.DateTimeField(auto_now_add=True)
    modified_Date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return unicode(self.vpc_OrderInfo)


