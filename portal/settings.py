"""
Django settings for portal project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ')w-=&tmolruioqbgc4v%pm8g75kj(t5(l0@^+&s#os7sp1m-l7'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'portal',
    'password_reset',
    #'connect2crm',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

ROOT_URLCONF = 'portal.urls'

WSGI_APPLICATION = 'portal.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'portal',
        'USER': 'root',
        'PASSWORD': '3x1651a',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Bahrain'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
#STATIC_ROOT = '/home/binny/portal/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    '/home/binny/portal/static/',
)

#Vtiger Credentials
CRM_URL = 'https://dev-crm.i.2connect.net/vtigerdev/webservice.php'
CRM_USER_NAME = 'admin'
CRM_USER_TOKEN = "cVDOx0Qg8KH9nMrC"
CRM_SITE_URL = 'http://dev-crm.i.2connect.net/'

CRM_MODULE_MAPPING = {'account':{'module':'Accounts', 'tab_id':'11x', 'quickbooks_id_tag':'cf_809', 'edit_sequence_tag':'cf_813',
                                'qb_status_tag':'cf_898', 'table_name':'vtiger_accountscf', 'portal_username_tag':'cf_992',
                                'portal_password_tag':'cf_994'},
                      'contact':{'module':'Contacts', 'tab_id':'12x', 'quickbooks_id_tag':'', 'edit_sequence_tag':'', 'qb_status_tag':'',
                                'table_name':'vtiger_contactscf'},
                      'potential':{'module':'Potentials', 'tab_id':'13x', 'quickbooks_id_tag':'cf_894', 'edit_sequence_tag':'cf_896',
                                'qb_status_tag':'cf_900', 'table_name':'vtiger_potentialscf'},
                      'salesorder':{'module':'SalesOrder', 'tab_id':'6x', 'quickbooks_id_tag':'cf_817', 'edit_sequence_tag':'cf_819',
                                'txn_line_id_tag':'cf_821', 'qb_status_tag':'cf_892', 'table_name':'vtiger_salesordercf'},
                      'product':{'module':'Products', 'tab_id':'14x', 'quickbooks_id_tag':'cf_847', 'edit_sequence_tag':'cf_849',
                                'txn_line_id_tag':'cf_851', 'qb_status_tag':'cf_902', 'table_name':'vtiger_productcf'},
                      'invoice':{'module':'Invoice', 'tab_id':'7x', 'quickbooks_id_tag':'cf_841', 'edit_sequence_tag':'cf_843',
                                'txn_line_id_tag':'cf_845', 'qb_status_tag':'cf_906', 'table_name':'vtiger_invoicecf'},
                      'transaction':{'module':'Transactions', 'tab_id':'41x', 'txn_payid_tag':'cf_1047', 'txn_orderid_tag':'cf_1049', 
                                 'txn_status_tag':'cf_1051', 'table_name':'vtiger_invoicecf'},
                      }

PRODUCTS = {'6x23544':{'name':'Residential BItstream 2Mbps (15GB-TH) - Staff','number':'PRO285','family':'Bitstream','unit_price':'0','recordid':'6x23544','order':1,'package_id':705}, '6x23547':{'name':'Residential BItstream 4Mbps (25GB-TH) - Staff','number':'PRO286','family':'Bitstream','unit_price':'10','recordid':'6x23547','order':2,'package_id':706}, '6x23548':{'name':'Residential BItstream 6Mbps (45GB-TH) - Staff','number':'PRO287','family':'Bitstream','unit_price':'15','recordid':'6x23548','order':3,'package_id':707}, '6x23549':{'name':'Residential BItstream 8Mbps (60GB-TH) - Staff','number':'PRO288','family':'Bitstream','unit_price':'20','recordid':'6x23549','order':4,'package_id':708}, '6x23550':{'name':'Residential BItstream 10Mbps (80GB-TH) - Staff','number':'PRO289','family':'Bitstream','unit_price':'25','recordid':'6x23550','order':5,'package_id':709}, '6x23551':{'name':'Residential BItstream 12Mbps (90GB-TH) - Staff','number':'PRO290','family':'Bitstream','unit_price':'30','recordid':'6x23551','order':6,'package_id':710}, '6x23552':{'name':'Residential BItstream 16Mbps (100GB-TH) - Staff','number':'PRO291','family':'Bitstream','unit_price':'40','recordid':'6x23552','order':7,'package_id':711}}

#Email Settings
DEFAULT_FROM_EMAIL = 'crm@i.2connect.net'
EMAIL_HOST = 'mail.i.2connectbahrain.com'
EMAIL_PORT = 25

#Site Settings
SITE_URL = 'http://192.168.2.95:8001'
SITE_ID = 2

#VPC_Settings
VPC_PREFIX = "2CON"
VPC_TITLE = "VPC 3-Party"
VPC_SECURE_SECRET = "147076428B743AFF3BB763F9EE7C9BE7"
VPC_URL = "https://migs.mastercard.com.au/vpcpay"
VPC_VERSION = "1"
VPC_COMMAND = "pay"
VPC_LOCALE = "en"
VPC_ACCESSCODE = "91193AFF"
VPC_MERCHANT = "E05061900"

