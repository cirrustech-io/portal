from django import template
register = template.Library()


@register.filter
def split(strng, splitter):
    return strng.split(splitter)

@register.filter
def fetch(val, index):
    return val[int(index)]

