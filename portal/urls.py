from django.conf.urls import patterns, include, url
from django.contrib import admin

app_name = 'portal'

urlpatterns = patterns('',
    # django rest api framwork urls
    #url(r'^', include(router.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^'+app_name+'/password-reset/', include('password_reset.urls')),
    url(r'^'+app_name+'/login/$', 'portal.views.login_user', name='login_user'),
    url(r'^'+app_name+'/logout/$', 'portal.views.logout_user', name='logout_user'),
    url(r'^'+app_name+'/dashboard/(?P<accountid>\w+)/$', 'portal.views.dashboard', name='dashboard'),
    url(r'^'+app_name+'/home/(?P<accountid>\w+)/$', 'portal.views.home', name='home'),
    url(r'^'+app_name+'/home/(?P<usertype>\w+)/(?P<page>\d+)/$', 'portal.views.home_staff', name='home_staff'),
    url(r'^'+app_name+'/info/(?P<module>\w+)/(?P<action>\w+)/(?P<moduleid>\w+)/$', 'portal.views.info', name='info'),
    url(r'^'+app_name+'/submit/(?P<module>\w+)/(?P<action>\w+)/(?P<moduleid>\w+)/$', 'portal.views.submit', name='submit'),
    url(r'^'+app_name+'/allcontact/(?P<accountid>\w+)/$', 'portal.views.all_contact', name='all_contact'),
    url(r'^'+app_name+'/allservice/(?P<accountid>\w+)/$', 'portal.views.all_service', name='all_service'),
    url(r'^'+app_name+'/service/(?P<orderid>\w+)/$', 'portal.views.service_details', name='service_details'),
    url(r'^'+app_name+'/service/(?P<action>\w+)/(?P<orderid>\w+)/$', 'portal.views.sales_action', name='sales_action'),
    url(r'^'+app_name+'/service/(?P<action>\w+)/(?P<orderid>\w+)/(?P<productid>\w+)/$', 'portal.views.service_change', name='service_change'),
    url(r'^'+app_name+'/newservice/(?P<accountid>\w+)/$', 'portal.views.new_service', name='new_service'),
    url(r'^'+app_name+'/submitservice/(?P<accountid>\w+)/$', 'portal.views.submit_service', name='submit_service'),
    url(r'^'+app_name+'/invoice/(?P<invoiceid>\w+)/$', 'portal.views.invoice', name='invoice'),
    url(r'^'+app_name+'/payment/(?P<action>\w+)/(?P<accountid>\w+)/$', 'portal.views.pre_payment', name='pre_payment'),
    url(r'^'+app_name+'/paymentsubmit/(?P<accountid>\w+)/$', 'portal.views.submit_payment', name='submit_payment'),
    url(r'^'+app_name+'/paymentcallback/(?P<accountid>\w+)/$', 'portal.views.call_back', name='call_back'),
    url(r'^'+app_name+'/transaction/(?P<accountid>\w+)/$', 'portal.views.get_transactions', name='get_transactions'),
    url(r'^'+app_name+'/contactus/(?P<accountid>\w+)/$', 'portal.views.contactus', name='contactus'),
    url(r'^'+app_name+'/contactus/$', 'portal.views.contactus2', name='contactus2'),
    url(r'^'+app_name+'/usage/(?P<accountid>\w+)/$', 'portal.views.usage_summary', name='usage_summary'),
    url(r'^'+app_name+'/$', 'portal.views.index', name='index'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
