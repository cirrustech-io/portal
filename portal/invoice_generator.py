#!/usr/bin/eCRM_nv python
"""
==============================================================================
title           : invoice_generator.py
description     : This is PDF invoice generator file
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python invoice_generator.py
notes           :
python_version  : 2.7.9
==============================================================================
"""

import pdfkit
from datetime import datetime
from settings import CRM_SITE_URL

def generate_invoice(data, invoice_path=None, routing_key=None, invoice_flag=False):

    invoice_html, product_name, invoice_num, reciever_mail = generate_invoice_html(data, invoice_flag)
    invoice_pdf = generate_invoice_pdf(invoice_html, product_name, invoice_num, invoice_path)
    #attachment_mailer(product_name, invoice_num, reciever_mail)
    return invoice_pdf

def generate_invoice_html(data, invoice_flag):

    account_data = data['payload']['account']
    account_name = account_data['accountname']
    reciever_mail = account_data['email1']
    invoice_data = data['payload']['invoice']
    invoice_current = 0.0
    invoice_balance = 0.0
    invoice_total = 0.0

    if invoice_flag:
        invoice_num = account_data['account_no']
        account_detail_name = account_name
        address_street = account_data['bill_street']
        address_city = account_data['bill_city']+""" """+account_data['bill_country']
        invoice_date = datetime.today().strftime("%d/%m/%Y")
        invoice_agree = account_data['id']
        invoice_due = ''
    else:
        contact_data = data['payload']['contact']
        salesorder_data = data['payload']['salesorder']
        invoice_data = data['payload']['invoice']
        product_data = data['payload']['product']

        invoice_num = invoice_data['invoice_no']
        account_detail_name = contact_data['firstname']+""" """+contact_data['lastname']
        address_street = invoice_data['bill_street']
        address_city = invoice_data['bill_city']+""" """+invoice_data['bill_country']
        invoice_date = invoice_data['invoicedate']
        invoice_agree = salesorder_data['potential_id']
        invoice_due = invoice_data['duedate']
        invoice_data = [invoice_data]

    invoice_html = """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <title>Invoice</title>
        <link rel='stylesheet' type='text/css' href='"""+CRM_SITE_URL+"""/test/invoice/EditableInvoice/css/style.css' />
        <link rel='stylesheet' type='text/css' href='"""+CRM_SITE_URL+"""/test/invoice/EditableInvoice/css/print.css' media="print" />
        <script type='text/javascript' src='"""+CRM_SITE_URL+"""/test/invoice/EditableInvoice/js/jquery-1.3.2.min.js'></script>
        <script type='text/javascript' src='"""+CRM_SITE_URL+"""/test/invoice/EditableInvoice/js/example.js'></script>
        <style>
          body {
            background-image:url('"""+CRM_SITE_URL+"""/test/logo/2Connect_Letterhead_small.jpg');
            background-repeat: repeat-x;
           }
        </style>
    </head>

    <body style="width:795px; height:1123px">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <div style="clear:both"></div>
        <div id="page-wrap">

            <div style="clear:both"></div>
            <div id="customer">
                <span style="float:left; border:1px solid; padding-left:5px;">
                        <b>Bill To</b>
                        <br />
                         """+account_name+"""
                        <br />
                         """+address_street+"""
                        <br />
                         """+address_city+"""
                </span>
                <span style="float:right">
                    <table id="meta">
                        <tr>
                            <td class="meta-head">Invoice #</td>
                            <td><textarea>"""+invoice_num+"""</textarea></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Invoice Date</td>
                            <td><textarea id="date">"""+invoice_date+"""</textarea></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Due Date</td>
                            <td><div class="due">"""+invoice_due+"""</div></td>
                        </tr>
                    </table>
                </span>
            </div>

            <table id="items">
                  <tr>
                      <th>No</th>
                      <th>Description</th>
                      <th>Quantity</th>
                      <th>Unit Cost</th>
                      <th>Price</th>
                  </tr>""";

    sq_no = 1
    for inv_dat in invoice_data:
        if inv_dat:
            for item in inv_dat['LineItems']:
                if item:
                    amount = float(item['quantity'])*float(item['listprice'])
                    invoice_html += """
                    <tr class="item-row">
                      <td>
                          """+str(sq_no)+""".
                      </td>
                      <td>
                        """+inv_dat['salesorder_id'].replace('6x','').strip()+""" - """+item['comment']+"""
                      </td>
                      <td>
                        """+str(int(float(item['quantity'])))+"""
                      </td>
                      <td>
                        """+str(float(item['listprice']))+"""
                      </td>
                      <td>
                        """+str(amount)+"""
                      </td>
                    </tr>"""

                    sq_no +=1
            invoice_current += float(inv_dat['hdnSubTotal'])
            invoice_balance += float(inv_dat['txtAdjustment'])
            invoice_total += float(inv_dat['hdnGrandTotal'])

    invoice_html += """
                  <tr>
                      <td colspan="2" class="blank"> </td>
                      <td colspan="2" class="total-line">Total Current Charges</td>
                      <td class="total-value"><div id="subtotal">"""+str(invoice_current)+"""</div></td>
                  </tr>
                  <tr>
                      <td colspan="2" class="blank"> </td>
                      <td colspan="2" class="total-line">Total Outstanding Balance</td>
                      <td class="total-value"><div id="total">"""+str(invoice_balance)+"""</div></td>
                  </tr>
                  <tr>
                      <td colspan="2" class="blank"> </td>
                      <td colspan="2" class="total-line balance">Balance Due</td>
                      <td class="total-value balance"><div class="due">"""+str(invoice_total)+"""</div></td>
                  </tr>

            </table>

            <div id="terms">
                <span style="padding-left:5px;">
                  Payment Mode: Cash/CHQ/Credit Card
                  <br />
                  Pay by cash/chq at: NBB Tower, 12th Flr, Manama
                </span>
            </div>
        </div>
    </body>
</html>"""

    return invoice_html, account_name, invoice_num, reciever_mail

def generate_invoice_pdfcrowd(invoice_html):
    import pdfcrowd
    client = pdfcrowd.Client("binnyabraham", "5a766cbae152f3ec5783eeb7d5807347")
    output_file = open('invoice_out.pdf', 'wb')
    client.convertHtml(invoice_html, output_file)
    output_file.close()
    return invoice_html

def generate_invoice_pdf(invoice_html, product_name, invoice_num, invoice_path):
     invoice_file = generate_invoice_file_path(product_name, invoice_num, '.pdf', invoice_path)
     pdfkit.from_string(invoice_html, invoice_file)
     return invoice_file

def generate_invoice_file_path(product_name, invoice_num, file_ext, invoice_path):
     return invoice_path + invoice_num.replace(' ','_').strip() + '_' +product_name.replace(' ','_').strip() + file_ext
