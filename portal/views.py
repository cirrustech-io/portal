
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render, redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from vtiger_connect import get_account, get_retrieve, get_query, get_create, get_describe, get_invoke, get_update, get_all_accounts, get_all_contact
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
import settings
import json
from rmqTask import Task
from invoice_generator import generate_invoice
from urllib import quote_plus
from hashlib import md5
from portal.models import PaymentDetails

task = Task()

acc_tab_id = settings.CRM_MODULE_MAPPING['account']['tab_id']
con_tab_id = settings.CRM_MODULE_MAPPING['contact']['tab_id']
pot_tab_id = settings.CRM_MODULE_MAPPING['potential']['tab_id']
sal_tab_id = settings.CRM_MODULE_MAPPING['salesorder']['tab_id']
prd_tab_id = settings.CRM_MODULE_MAPPING['product']['tab_id']
inv_tab_id = settings.CRM_MODULE_MAPPING['invoice']['tab_id']

@csrf_protect
def login_user(request):
    state = "Please log in below..."
    username = password = ''

    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:

	    # Check for Staff Access to the portal
            #if(user.is_staff):
            #    login(request, user)
            #    return redirect('/home/staff/1/')

	    # Check for the account id in user profile table in portal database
	    try:
	        user_profile = user.userprofile
	        profile_flag = True
	    except:
	        profile_flag = False

	    # User login in site portal after account check in crm
	    if ((profile_flag) and (user.is_active) and (user.userprofile.vtiger is not None and user.userprofile.vtiger != '')):
	        user_acc = get_retrieve(acc_tab_id+user.userprofile.vtiger)
		if((user_acc) and (user_acc[settings.CRM_MODULE_MAPPING['account']['portal_username_tag']].strip() == user.username) and (user_acc[settings.CRM_MODULE_MAPPING['account']['portal_password_tag']].strip() == user.password)):
	            login(request, user)
	            return redirect('dashboard', accountid=user.userprofile.vtiger)

    return HttpResponseRedirect('/portal/?error=login fails')

@login_required(login_url='/portal/')
def dashboard(request, accountid):

    next_url = ''
    primary_contact = None
    if request.META.has_key('HTTP_REFERER') and 'next' in request.META['HTTP_REFERER']:
        next_url = request.META['HTTP_REFERER'].split('next=')[1].split('&')[0]
        if '/portal/dashboard/' in next_url:
            next_url=''

    exact_acc_id = get_exact_account_id(request, accountid)
    account = get_retrieve(exact_acc_id)
    con_query = "SELECT * From Contacts WHERE account_id='%s';" % (exact_acc_id)
    contacts = get_query(con_query)
    if contacts:
        primary_contact = contacts[0]

    if account:
    	state = "You're successfully logged in!"
    else:
	state = 'An error in login. Please try again'
	return HttpResponseRedirect('/portal/?error=login fails')

    if(next_url):
        return HttpResponseRedirect(next_url)
    else:
        return render_to_response('portal/dashboard.html', {'account': account, 'state':state, 'accountid':exact_acc_id, 'primary_contact':primary_contact}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def home_staff(request, usertype, page):

    next_url = ''
    if request.META.has_key('HTTP_REFERER') and 'next' in request.META['HTTP_REFERER']:
        next_url = request.META['HTTP_REFERER'].split('next=')[1].split('&')[0]
        if next_url == '/portal/home/staff/1/':
            next_url=''
    state = "You're successfully logged in!"
    count = int(page) - 1
    start = str(count) + '01'
    special_accnts = []
    special_accnts.append(get_retrieve('3x36567'))
    special_accnts.append(get_retrieve('3x36568'))
    records = get_all_accounts(start)
    if records.has_key('error'):
        state = records['error']
    else:
        if(next_url):
            return HttpResponseRedirect(next_url)
        else:
            return render_to_response('portal/home_staff.html', {'special_accounts':special_accnts, 'accounts': records['accounts'], 'state':state}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def home(request, accountid):

    next_url = ''
    if request.META.has_key('HTTP_REFERER') and 'next' in request.META['HTTP_REFERER']:
        next_url = request.META['HTTP_REFERER'].split('next=')[1].split('&')[0]
        if next_url == '/portal/home/'+accountid+'/':
            next_url=''
    state = "You're successfully logged in!"
    exact_acc_id = get_exact_account_id(request, accountid)
    records = get_account(exact_acc_id)
    print records
    sales_dict = []
    if records.has_key('error'):
        state = records['error']
    else:
        if(next_url):
            return HttpResponseRedirect(next_url)
        else:
            sales_dict = avoid_duplicate(records['sales_orders'])
    return render_to_response('portal/account_details.html', {'account': records['account'], 'sales_orders':sales_dict, 'state':state, 'accountid':exact_acc_id}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def all_contact(request, accountid):
    exact_acc_id = get_exact_account_id(request, accountid)
    account = get_account(exact_acc_id)
    contacts = get_all_contact(exact_acc_id)
    return render_to_response('portal/contacts.html', {'account': account, 'contacts':contacts, 'accountid':exact_acc_id}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def contactus(request, accountid):
    exact_acc_id = get_exact_account_id(request, accountid)
    return render_to_response('portal/contact2Connect.html', {'accountid':exact_acc_id}, context_instance=RequestContext(request),)

def contactus2(request):
    return render_to_response('portal/contact2Connect.html', {}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def usage_summary(request, accountid):
    exact_acc_id = get_exact_account_id(request, accountid)
    return render_to_response('portal/usage_summary.html', {'accountid':exact_acc_id}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def all_service(request, accountid):

    next_url = ''
    if request.META.has_key('HTTP_REFERER') and 'next' in request.META['HTTP_REFERER']:
        next_url = request.META['HTTP_REFERER'].split('next=')[1].split('&')[0]
    state = "You're successfully logged in!"
    exact_acc_id = get_exact_account_id(request, accountid)
    records = get_account(exact_acc_id)
    if records.has_key('error'):
        state = records['error']
    else:
        if(next_url):
            return HttpResponseRedirect(next_url)
        else:
            sales_dict = avoid_duplicate(records['sales_orders'])
            return render_to_response('portal/services.html', {'account': records['account'], 'sales_orders':sales_dict, 'state':state, 'accountid':exact_acc_id}, context_instance=RequestContext(request),)

def get_exact_account_id(request, accountid):
    if(request.user.is_staff):
        acc_id = accountid
    else:
        acc_id = request.user.userprofile.vtiger
    return get_exact_module_id(acc_id, 'account')

def get_exact_module_id(moduleid, module):
    if 'x' in moduleid:
        moduleid = moduleid.split('x')[1]
    return settings.CRM_MODULE_MAPPING[module]['tab_id']+moduleid

def avoid_dct_duplicate(lst):
    unique = set()
    for d in lst:
        t = tuple(d.iteritems())
        unique.add(t)
    return [dict(x) for x in unique]

def avoid_duplicate(lst):
    id_lst = []
    new_lst = []
    for dct in lst:
        if dct['id'] not in id_lst:
            id_lst.append(dct['id'])
            new_lst.append(dct)
    return new_lst

def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/portal/')

def index(request):
    logout(request)
    state = "Please log in below..."
    username = password = ''
    return render_to_response('portal/auth.html',{'state':state, 'username': username},
                              context_instance=RequestContext(request),)

def new_service(request, accountid):
    if (accountid == 'guest'):
        exact_acc_id = 'guest'
        account = False
        logout(request)
    else:
        exact_acc_id = get_exact_account_id(request, accountid)
        account = get_retrieve(exact_acc_id)
    products = settings.PRODUCTS
    sorted_products = sorted(products.items(), key=lambda x: x[1]['order'])
    return render_to_response('portal/req_new_service.html',{'account':account, 'accountid':exact_acc_id, 'sorted_products':sorted_products},
                              context_instance=RequestContext(request),)

def submit_service(request, accountid):

    productid = request.POST.get('productid','6x23544')
    data = {'product':settings.PRODUCTS[productid],'account':{}}
    if(accountid == 'guest'):
        for k, v in dict(request.POST).items():
            data['account'][k] = "".join(v)
    else:
        exact_acc_id = get_exact_account_id(request, accountid)
        data['account'] = get_retrieve(exact_acc_id)
        accountid = exact_acc_id
    message = json.dumps(data)
    task.publish_message(message, 'newservice')
    return render_to_response('portal/new_service_submit.html',{'message':message, 'accountid':accountid},
                              context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def info(request, module, action, moduleid):
    if (module == 'account'):
        exact_acc_id = get_exact_account_id(request, moduleid)
        account = get_retrieve(exact_acc_id)
	render_html = 'portal/edit_account.html'
    else:
        account = ''
        if(action == 'edit'):
	    account = get_retrieve(moduleid)
	    exact_acc_id = account['account_id']
        elif(action == 'add'):
            exact_acc_id = moduleid
	render_html = 'portal/edit_contact.html'
    return render_to_response(render_html, {'account':account, 'module':module, 'action': action, 'accountid':exact_acc_id},
                              context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def submit(request, module, action, moduleid):
    moduleid = get_exact_module_id(moduleid, module)
    if (action =='edit'):
        module_data = get_retrieve(moduleid)
    else:
        moduleid = get_exact_module_id(moduleid, 'account')
        account_data = get_retrieve(moduleid)
        module_data = {}
    next_url = ''
    potential_id = ''
    if request.META.has_key('HTTP_REFERER') and 'next' in request.META['HTTP_REFERER']:
        next_url = request.META['HTTP_REFERER'].split('next=')[1].split('&')[0]
    if request.META.has_key('HTTP_REFERER') and 'potential' in request.META['HTTP_REFERER']:
        potential_id = request.META['HTTP_REFERER'].split('potential=')[1].split('&')[0]
        con_type = request.META['HTTP_REFERER'].split('type=')[1].split('&')[0]

    if request.POST:
        post_val = request.POST
	if (module == 'account'):
	    accountid = moduleid
	    render_html = 'account_details'
            module_data['phone'] = post_val['phone']
            module_data['otherphone'] = post_val['otherphone']
            module_data['website'] = post_val['website']
            module_data['fax'] = post_val['fax']
            module_data['email2'] = post_val['email2']
	else:
	    module_data['title'] = post_val['title']
	    module_data['firstname'] = post_val['fname']
	    module_data['lastname'] = post_val['lname']
	    module_data['phone'] = post_val['phone']
	    module_data['mobile'] = post_val['mobile']
	    module_data['fax'] = post_val['fax']
	    module_data['email'] = post_val['email']
	    module_data['mailingstreet'] = post_val['address']
	    module_data['mailingpobox'] = post_val['pobox']
            if action == 'edit':
	        accountid = module_data['account_id']
            else:
                accountid = get_exact_module_id(moduleid, 'account')
	    render_html = 'all_contact'
    if (action == 'edit'):
        updateInfo = get_update(settings.CRM_MODULE_MAPPING[module]['module'], module_data)
    else:
        module_data['account_id'] = accountid
        module_data['created_user_id'] = account_data['created_user_id']
        module_data['assigned_user_id'] = account_data['assigned_user_id']
        module_data['modifiedby'] = account_data['modifiedby']
        updateInfo = get_create(settings.CRM_MODULE_MAPPING[module]['module'], module_data)
        if(potential_id):
            con_dct = {'tech':{'field':'cf_984'}, 'bill':{'field':'cf_986'}}
            pot_data = get_retrieve(potential_id)
            pot_data[con_dct[con_type]['field']] = updateInfo['id']
            get_update(settings.CRM_MODULE_MAPPING['potential']['module'], pot_data)

    if(next_url):
        return HttpResponseRedirect(next_url)

    return redirect(render_html, accountid=accountid)

@login_required(login_url='/portal/')
def invoice(request, invoiceid):
    data = {'payload':{}}
    invoice = get_retrieve(invoiceid)
    data['payload']['invoice'] = invoice
    accountid = get_exact_account_id(request, invoice['account_id'])
    data['payload']['salesorder'] = get_retrieve(invoice['salesorder_id'])
    data['payload']['account'] = get_retrieve(invoice['account_id'])
    data['payload']['contact'] = get_retrieve(invoice['contact_id'])
    data['payload']['product'] = get_retrieve(data['payload']['invoice']['LineItems'][0]['productid'])
    invoice_path = settings.STATICFILES_DIRS[0] + '/portal/invoice/'
    invoice_pdf = generate_invoice(data, invoice_path=invoice_path)
    with open(invoice_pdf, 'r') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'filename='+invoice_pdf
        return response

@login_required(login_url='/portal/')
def service_details(request, orderid):
    sale_order = get_retrieve(orderid)
    accountid = get_exact_account_id(request, sale_order['account_id'])
    account_manager = get_retrieve(sale_order['assigned_user_id'])
    potential = get_retrieve(sale_order['potential_id'])
    tech_contact = get_retrieve(potential['cf_984'])
    bill_contact = get_retrieve(potential['cf_986'])
    action_list = [{"name":"Edit", "action":"edit", "active":False, "url":"/service/edit/%s/"%(orderid)},
                   {"name":"Upgrade", "action":"update", "active":False, "url":"/service/upgrade/%s/"%(orderid)},
                   {"name":"Downgrade", "action":"downgrade", "active":False, "url":"/service/downgrade/%s/"%(orderid)},
                   {"name":"Topup Quota", "action":"quota", "active":False, "url":"/service/quota/%s/"%(orderid)},
		   {"name":"Update Package", "action":"update", "active":False, "url":"/service/update/%s/"%(orderid)},
                   {"name":"View Invoices", "action":"invoice", "active":True, "url":"/service/invoice/%s/"%(orderid)},
                   {"name":"View Usage", "action":"usage", "active":True, "url":"/service/usage/%s/"%(orderid)},
                   {"name":"Make Payment", "action":"payment", "active":True, "url":"/service/payment/%s/"%(orderid)},
                   {"name":"Change Subscription", "action":"subscribe", "active":True, "url":"/service/subscribe/%s/"%(orderid)},
                   {"name":"Reset Password", "action":"reset", "active":True, "url":"/service/reset/%s/"%(orderid)},
                   {"name":"Update Contacts", "action":"", "active":False, "url":"/contactupdate/%s/"%(orderid)},
                   {"name":"Provision Subscribe", "action":"provision", "active":False, "url":"/service/provision/%s/"%(orderid)},
		]
    return render_to_response('portal/service_details.html',{'service_action':action_list, 'orderid':orderid, 'sale_order':sale_order, 'account_manager':account_manager, 'accountid':accountid, 'tech_contact':tech_contact, 'bill_contact':bill_contact}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def sales_action(request, action, orderid):

    products = settings.PRODUCTS
    sorted_products = sorted(products.items(), key=lambda x: x[1]['order'])
    sale_order = get_retrieve(orderid)
    if(sale_order):
        accountid = get_exact_account_id(request, sale_order['account_id'])
    
    render_html = 'portal/sales_action.html'
    action_name_map = {'provision':{'name':'Provision Subscribe', 'render_html':render_html},
                       'update':{'name':'Update Package', 'render_html':render_html},
                       'reset':{'name':'Reset Password', 'render_html':render_html},
                       'quota':{'name':'Topup Quota', 'render_html':render_html},
                       'downgrade':{'name':'Downgrade', 'render_html':render_html},
                       'upgrade':{'name':'Upgrade', 'render_html':render_html},
                       'invoice':{'name':'View Invoices', 'render_html':'portal/billing.html'},
                       'edit':{'name':'Edit', 'render_html':render_html},
                       'payment':{'name':'Make Payment', 'render_html':'portal/billing.html'},
                       'contactupdate':{'name':'Update Contacts', 'render_html':render_html},
                       'subscribe':{'name':'Change Subscription', 'render_html':render_html},
                       'usage':{'name':'View Usage', 'render_html':'portal/usage_summary.html'},
		      }
    action_name = action_name_map[action]['name']
    quota_options = {'5 GB':5000000, '10 GB':10000000, '15 GB':15000000, '30 GB':30000000}
    current_product = {}

    invoice = ''
    if ((action == 'invoice') or (action == 'payment')):
        if '11x' in orderid:
            accountid = orderid
	    inv_query = "SELECT * From Invoice WHERE account_id='%s';" % (orderid)
        else:
	    inv_query = "SELECT * From Invoice WHERE salesorder_id='%s';" % (orderid)
	invoice = get_query(inv_query)

    return render_to_response(action_name_map[action]['render_html'], {'current_product':current_product,'products':products, 'sorted_products':sorted_products, 'action':action, 'orderid':orderid, 'sale_order':sale_order, 'action_name':action_name, 'quota_options':quota_options, 'invoice':invoice, 'accountid':accountid}, context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def pre_payment(request, action, accountid):
    #invoiceArray = eval(str(request.POST['invoiceArray']))
    accountid = get_exact_account_id(request, accountid)
    invoiceArray = None
    if type(invoiceArray) == dict:
        inv_lst = []
        inv_lst.append(invoiceArray)
        invoiceArray = inv_lst
    return render_to_response('portal/pre_payment.html', {'invoiceArray':invoiceArray, 'action':action, 'accountid':accountid} , context_instance=RequestContext(request),)

def submit_payment(request, accountid):

    accountid = get_exact_account_id(request, accountid)
    SECURE_SECRET = settings.VPC_SECURE_SECRET

    post_data = request.POST.copy()
    invoiceArray = eval(str(post_data['invoiceArray']))

    #Create Random Order ID
    import uuid
    orderId = (settings.VPC_PREFIX + '_' + str(uuid.uuid4()).split('-')[0] + '_ODR').upper()
    txnRef = orderId + '-1'
    amount = 10
    
    PAY_POST = {"Title":settings.VPC_TITLE, "virtualPaymentClientURL":settings.VPC_URL, "vpc_Version":settings.VPC_VERSION, "vpc_Command":settings.VPC_COMMAND, "vpc_AccessCode":settings.VPC_ACCESSCODE, "vpc_Merchant":settings.VPC_MERCHANT, "vpc_Locale":settings.VPC_LOCALE, "vpc_MerchTxnRef":txnRef, "vpc_OrderInfo":orderId, "vpc_Amount":str(int(round(amount))), "vpc_ReturnURL":settings.SITE_URL+"/portal/paymentcallback/"+accountid+"/"}

    # add the start of the vpcURL querystring parameters
    vpcURL = PAY_POST["virtualPaymentClientURL"] + "?"

    # Remove the Virtual Payment Client URL from the parameter hash as we 
    # do not want to send these fields to the Virtual Payment Client.
    del PAY_POST["virtualPaymentClientURL"]
    if PAY_POST.has_key('SubButL'):
        del PAY_POST["SubButL"]

    # The URL link for the receipt to do another transaction.
    # Note: This is ONLY used for this example and is not required for 
    # production code. You would hard code your own URL into your application.

    # Get and URL Encode the AgainLink. Add the AgainLink to the array
    # Shows how a user field (such as application SessionIDs) could be added
    #PAY_POST['AgainLink']=quote_plus(HTTP_REFERER)

    # Create the request to the Virtual Payment Client which is a URL encoded GET
    # request. Since we are looping through all the data we may as well sort it in
    # we want to create a secure hash and add it to the VPC data if the
    # merchant secret has been provided.
    md5HashData = SECURE_SECRET
    PAY_POST_TUPL = ksort(PAY_POST)

    # set a parameter to show the first pair in the URL
    appendAmp = 0

    for item in PAY_POST_TUPL:
        # create the md5 input and URL leaving out any fields that have no value
        key = item[0]
        value = item[1]
        if (len(value) > 0 and (key.startswith('user_') or key.startswith('vpc_'))):
            # this ensures the first paramter of the URL is preceded by the '?' char
            if (appendAmp == 0):
                vpcURL += quote_plus(key) + '=' + quote_plus(value)
                appendAmp = 1
            else:
                vpcURL += '&' + quote_plus(key) + "=" + quote_plus(value)
            md5HashData += value

    # Create the secure hash and append it to the Virtual Payment Client Data if
    # the merchant secret has been provided.
    if (len(SECURE_SECRET) > 0):
        secureHash = md5(md5HashData).hexdigest().upper()
        vpcURL += "&vpc_SecureHash=" + secureHash

    #Insert the transaction details into payment Details Table
    pdObj = PaymentDetails(user=request.user, account_Id=accountid, vpc_OrderInfo=orderId, vpc_MerchTxnRef=txnRef, vpc_Amount=float(amount), vpc_SecureHash=secureHash, payment_Status='Initiated')
    pdObj.save()

    #Insert the details into transaction table in CRM
    if (type(invoiceArray) != tuple):
        invoiceArray = [invoiceArray]
    id_lst = []
    for inv in invoiceArray:
        inv_id = inv['inv_id'].split('@@@')[0]
        inv_subj = inv['inv_id'].split('@@@')[2]
        txn_details = {'txnaccountid': accountid, 
                   'assigned_user_id': '19x1', 
                   'txninvoiceid': inv_id, 
                   'paymentdate': '', 
                   'amountpaid': inv['des_amt'], 
                   'subject': inv_subj,
                   settings.CRM_MODULE_MAPPING['transaction']['txn_payid_tag']: str(int(pdObj.id)), 
                   settings.CRM_MODULE_MAPPING['transaction']['txn_orderid_tag']: orderId, 
                   settings.CRM_MODULE_MAPPING['transaction']['txn_status_tag']: 'Initiated', 
                   }
        txn_rec = get_create(settings.CRM_MODULE_MAPPING['transaction']['module'], txn_details)
        id_lst.append(txn_rec['id'])

    pdObj.txn_Ids = json.dumps(id_lst)
    pdObj.save()

    # FINISH TRANSACTION - Redirect the customers using the Digital Order
    # ===================================================================
    #header("Location: "+vpcURL)
    return redirect(vpcURL)

def call_back(request, accountid):

    accountid = get_exact_account_id(request, accountid)
    SECURE_SECRET = settings.VPC_SECURE_SECRET
    PAY_GET = request.GET.copy()

    # If there has been a merchant secret set then sort and loop through all the
    # data in the Virtual Payment Client response. While we have the data, we can
    # append all the fields that contain values (except the secure hash) so that
    # we can create a hash and validate it against the secure hash in the Virtual
    # Payment Client response.

    # NOTE: If the vpc_TxnResponseCode in not a single character then
    # there was a Virtual Payment Client error and we cannot accurately validate
    # the incoming data from the secure hash. */

    # get and remove the vpc_TxnResponseCode code from the response fields as we
    # do not want to include this field in the hash calculation
    vpc_Txn_Secure_Hash = PAY_GET["vpc_SecureHash"]
    del PAY_GET["vpc_SecureHash"]

    # set a flag to indicate if hash has been validated
    errorExists = False

    if (len(SECURE_SECRET) > 0 and PAY_GET["vpc_TxnResponseCode"] != "7" and PAY_GET["vpc_TxnResponseCode"] != "No Value Returned"):

        md5HashData = SECURE_SECRET

        # sort all the incoming vpc response fields and leave out any with no value
        PAY_GET_TUPL = ksort(PAY_GET)
        for item in PAY_GET_TUPL:
            key = item[0]
            value = item[1]
            if (key != "vpc_SecureHash" or len(value) > 0):
                md5HashData += value
    
        # Validate the Secure Hash (remember MD5 hashes are not sensitive)
	    # This is just one way of displaying the result of checking the hash.
	    # In production, you would work out your own way of presenting the result.
	    # The hash check is all about detecting if the data has changed in transit.
        if (vpc_Txn_Secure_Hash.upper() == md5(md5HashData).hexdigest().upper()):
            # Secure Hash validation succeeded, add a data field to be displayed
            hashValidated = "CORRECT"
        else:
            # Secure Hash validation failed, add a data field to be displayed
            hashValidated = "INVALID HASH"
            errorExists = True
    else:
        # Secure Hash was not validated, add a data field to be displayed later.
        hashValidated = "Not Calculated - No SECURE_SECRET"

    print hashValidated

    # Define Variables
    # ----------------
    # Extract the available receipt fields from the VPC Response
    # If not present then let the value be equal to 'No Value Returned'

    # Standard Receipt Data
    amount          = null2unknown(PAY_GET["vpc_Amount"])
    locale          = null2unknown(PAY_GET["vpc_Locale"])
    batchNo         = null2unknown(PAY_GET["vpc_BatchNo"])
    command         = null2unknown(PAY_GET["vpc_Command"])
    message         = null2unknown(PAY_GET["vpc_Message"])
    version         = null2unknown(PAY_GET["vpc_Version"])
    cardType        = null2unknown(PAY_GET["vpc_Card"]) if PAY_GET.has_key('vpc_Card') else ""
    orderInfo       = null2unknown(PAY_GET["vpc_OrderInfo"])
    receiptNo       = null2unknown(PAY_GET["vpc_ReceiptNo"]) if PAY_GET.has_key('vpc_ReceiptNo') else ""
    merchantID      = null2unknown(PAY_GET["vpc_Merchant"])
    authorizeID     = null2unknown(PAY_GET["vpc_AuthorizeId"]) if PAY_GET.has_key('vpc_AuthorizeId') else ""
    merchTxnRef     = null2unknown(PAY_GET["vpc_MerchTxnRef"])
    transactionNo   = null2unknown(PAY_GET["vpc_TransactionNo"])
    acqResponseCode = null2unknown(PAY_GET["vpc_AcqResponseCode"]) if PAY_GET.has_key('vpc_AcqResponseCode') else ""
    txnResponseCode = null2unknown(PAY_GET["vpc_TxnResponseCode"]) if PAY_GET.has_key('vpc_TxnResponseCode') else ""
   
    responseDesc = getResponseDescription(txnResponseCode)

    # 3-D Secure Data
    verType         = PAY_GET["vpc_VerType"] if PAY_GET.has_key('vpc_VerType') else "No Value Returned"
    verStatus       = PAY_GET["vpc_VerStatus"] if PAY_GET.has_key('vpc_VerStatus') else "No Value Returned"
    token           = PAY_GET["vpc_VerToken"] if PAY_GET.has_key('vpc_VerToken') else "No Value Returned"
    verSecurLevel   = PAY_GET["vpc_VerSecurityLevel"] if PAY_GET.has_key('vpc_VerSecurityLevel') else "No Value Returned"
    enrolled        = PAY_GET["vpc_3DSenrolled"] if PAY_GET.has_key('vpc_3DSenrolled') else "No Value Returned"
    xid             = PAY_GET["vpc_3DSXID"] if PAY_GET.has_key('vpc_3DSXID') else "No Value Returned"
    acqECI          = PAY_GET["vpc_3DSECI"] if PAY_GET.has_key('vpc_3DSECI') else "No Value Returned"
    authStatus      = PAY_GET["vpc_3DSstatus"] if PAY_GET.has_key('vpc_3DSstatus') else "No Value Returned"
    
    statusDesc = getStatusDescription(verStatus)

    # *******************
    # END OF MAIN PROGRAM
    # *******************

    # FINISH TRANSACTION - Process the VPC Response Data
    # =====================================================
    # For the purposes of demonstration, we simply display the Result fields on a
    # web page.

    # Show 'Error' in title if an error condition
    errorTxt = ""
    pg_error_flag = False
    # Show this page as an error page if vpc_TxnResponseCode equals '7'
    if (txnResponseCode == "7" or txnResponseCode == "No Value Returned" or errorExists):
        errorTxt = "Error"
        pg_error_flag = True

    hash_validated_flag = False
    if(hashValidated == 'CORRECT'):
        hash_validated_flag = True
    
    # This is the display title for 'Receipt' page 
    title = PAY_GET["Title"] if PAY_GET.has_key('Title') else "Receipt Page"

    #Update the Order Details in payment details table
    pdObj = PaymentDetails.objects.filter(vpc_OrderInfo=orderInfo)
    for pd in pdObj:
        pd.is_hashValidated = hash_validated_flag
        pd.payment_Status = message
        pd.vpc_BatchNo = batchNo
        pd.vpc_Message = message
        pd.vpc_Card = cardType
        pd.vpc_ReceiptNo = receiptNo
        pd.vpc_AuthorizeId = authorizeID
        pd.vpc_TransactionNo = transactionNo
        pd.vpc_AcqResponseCode = acqResponseCode
        pd.vpc_TxnResponseCode = txnResponseCode
        pd.vpc_TxnResponseDesc = responseDesc
        pd.vpc_VerStatus = verStatus
        pd.vpc_VerStatusDesc = statusDesc
        pd.vpc_VerType = verType
        pd.vpc_VerToken = token
        pd.vpc_VerSecurityLevel = verSecurLevel
        pd.vpc_3DSenrolled = enrolled
        pd.vpc_3DSXID = xid
        pd.vpc_3DSECI = acqECI
        pd.vpc_3DSstatus = authStatus
        pd.is_PGError = pg_error_flag
        pd.save()

        for txnid in json.loads(pd.txn_Ids):
            txn = get_retrieve(txnid)
            txn[settings.CRM_MODULE_MAPPING['transaction']['txn_status_tag']] = message
            txn['paymentdate'] = pd.modified_Date.strftime('%Y-%m-%d')
            txn = get_update(settings.CRM_MODULE_MAPPING['transaction']['module'],txn)
    
    # The URL link for the receipt to do another transaction.
    # Note: This is ONLY used for this example and is not required for 
    # production code. You would hard code your own URL into your application
    # to allow customers to try another transaction.
    #TK#againLink = URLDecode(PAY_GET["AgainLink"])

    return render_to_response('portal/post_payment.html', {'accountid':accountid, 'message':message, 'title':title, 'errorTxt':errorTxt, 'responseDesc':responseDesc, 'statusDesc':statusDesc, 'hashValidated':hashValidated, 'orderInfo':orderInfo}, context_instance=RequestContext(request),)

def ksort(d):
     return [(k,d[k]) for k in sorted(d.keys())]

# This method uses the QSI Response code retrieved from the Digital
# Receipt and returns an appropriate description for the QSI Response Code
# @param responseCode String containing the QSI Response Code
# @return String containing the appropriate description
def getResponseDescription(responseCode):
    return map_responseCode(responseCode)

def map_responseCode(responseCode):
    response_dct = {"0" : "Transaction Successful",
        "?" : "Transaction status is unknown",
        "1" : "Unknown Error",
        "2" : "Bank Declined Transaction",
        "3" : "No Reply from Bank",
        "4" : "Expired Card",
        "5" : "Insufficient funds",
        "6" : "Error Communicating with Bank",
        "7" : "Payment Server System Error",
        "8" : "Transaction Type Not Supported",
        "9" : "Bank declined transaction (Do not contact Bank)",
        "A" : "Transaction Aborted",
        "C" : "Transaction Cancelled",
        "D" : "Deferred transaction has been received and is awaiting processing",
        "F" : "3D Secure Authentication failed",
        "I" : "Card Security Code verification failed",
        "L" : "Shopping Transaction Locked (Please try the transaction again later)",
        "N" : "Cardholder is not enrolled in Authentication scheme",
        "P" : "Transaction has been received by the Payment Adaptor and is being processed",
        "R" : "Transaction was not processed - Reached limit of retry attempts allowed",
        "S" : "Duplicate SessionID (OrderInfo)",
        "T" : "Address Verification Failed",
        "U" : "Card Security Code Failed",
        "V" : "Address Verification and Card Security Code Failed",
        }

    try:
        response_desc = response_dct[responseCode]
    except:
        response_desc = "Unable to be determined"
    return response_desc


#  -----------------------------------------------------------------------------
# This method uses the verRes status code retrieved from the Digital
# Receipt and returns an appropriate description for the QSI Response Code

# @param statusResponse String containing the 3DS Authentication Status Code
# @return String containing the appropriate description

def getStatusDescription(statusResponse):
    if (statusResponse == "" or statusResponse == "No Value Returned"):
        result = "3DS not supported or there was no 3DS data provided"
    else:
        result = map_statusResponse(statusResponse)
    return result

def map_statusResponse(status):
    status_dict = {"Y":"The cardholder was successfully authenticated.",
                   "E":"The cardholder is not enrolled.",
                   "N":"The cardholder was not verified.",
                   "U":"The cardholder's Issuer was unable to authenticate due to some system error at the Issuer.",
                   "F":"There was an error in the format of the request from the merchant.",
                   "A":"Authentication of your Merchant ID and Password to the ACS Directory Failed.",
                   "D":"Error communicating with the Directory Server.",
                   "C":"The card type is not supported for authentication.",
                   "S":"The signature on the response received from the Issuer could not be validated.",
                   "P":"Error parsing input from Issuer.",
                   "I":"Internal Payment Server system error.",
                   }
    try:
        status_desc = status_dict[status]
    except:
        status_desc = "Unable to be determined"
    return status_desc

#  -----------------------------------------------------------------------------
# If input is null, returns string "No Value Returned", else returns input
def null2unknown(data):
    if(data == ""):
        return "No Value Returned"
    else:
        return data
#  ----------------------------------------------------------------------------

def get_transactions(request, accountid):

    inv_query = "SELECT * From Invoice WHERE account_id='%s';" % (accountid)
    invoice = get_query(inv_query)
    txn_query = "SELECT * From Transactions WHERE txnaccountid='%s';" % (accountid)
    transaction = get_query(txn_query)

    return render_to_response('portal/transaction_history.html', {'invoice':invoice, 'transaction':transaction, 'accountid':accountid} , context_instance=RequestContext(request),)

@login_required(login_url='/portal/')
def service_change(request, action, orderid, productid):
    qu_amount = 0
    if(request.POST.has_key('quota')):
       qu_amount = request.POST['quota']
    sale_order = get_retrieve(orderid)
    accountid = sale_order['account_id']
    new_sales_order = ''
    product = settings.PRODUCTS[productid]
    if (action == 'upgrade' or action == 'downgrade'):
        new_sales_order = create_update_saleorder(sale_order, product)
        accountid = new_sales_order['account_id']
    else:
        publish_queue(action, sale_order, product, qu_amount)
    action_response_map = {"reset":"Your password is updated and sent to your email",
                           "update":"Your package is updated successfully",
                           "provision":"Your provision is subscribed successfully",
                           "quota":"Your quota is updated successfully",
                           "edit":"",
                           "upgrade":"Your service is upgraded successfully",
                           "downgrade":"Your service is downgraded successfully"}
    return render_to_response('portal/new_sales.html', {'sales_order': new_sales_order, 'message':action_response_map[action], 'action':action, 'accountid':accountid} , context_instance=RequestContext(request),)

def publish_queue(action, sale_order, product, quota_amount=None):
    username = sale_order['cf_725']
    package_id = product['package_id']
    product_type = "Bitstream"
    action_message_map = {'reset':{"username":username},
                          'quota':{"username":username,"package_id":package_id,"quota_amount":quota_amount},
                          'update':{"username":username,"package_id":package_id},
                          'provision':{"username":username,"products":[{"product_type": product_type,"package_id":package_id}]}}
    message = json.dumps(action_message_map[action])
    task.publish_message(message, action)

def create_update_saleorder(sale_order, new_product):
    sale_order['invoicestatus'] = 'AutoCreated'
    new_record = sale_order.copy()
    module = 'SalesOrder'
    sale_order['sostatus'] = "inactive"
    sale_order['productid'] = sale_order['LineItems'][0]['productid']
    updateInfo = get_update(module, sale_order)
    new_record['subject'] = new_product['name']
    new_record['sostatus'] = 'Active'
    new_record['salesorder_no'] = ""
    del new_record['id']
    if new_record.has_key('LineItems'):
        new_item_lst = []
        for item in new_record['LineItems']:
            old_item = item.copy()
            if old_item.has_key('productid'):
                del old_item
                new_line_item = {u'productid': new_product['recordid'], u'listprice': new_product['unit_price'], u'quantity': u'1.000'}
                new_item_lst.append(new_line_item)
            else:
                new_item_lst.append(old_item)
        new_record['LineItems'] = new_item_lst

    new_record['productid'] = new_product['recordid']
    recordInfo = get_create(module, new_record)
    publish_queue('update', recordInfo, new_product)
    return recordInfo
