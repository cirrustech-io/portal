VIRTUAL_HOST = 'poc'
HOST_NAME = 'mq.2connectbahrain.com'
MAIN_EXCHANGE = {'name':'dsl', 'type':'topic'}
QUEUE_MAP = {'provision':{'name':'ProvisionSubscriber','type':'direct', 'routing_key':'ProvisionSubscriber'}, 'quota':{'name':'TopupQuota','type':'direct', 'routing_key':'TopupQuota'}, 'update':{'name':'UpdatePackage','type':'direct', 'routing_key':'UpdatePackage'}, 'reset':{'name':'ResetPassword','type':'direct', 'routing_key':'ResetPassword'}, 'newservice':{'name':'SendEmail','type':'direct', 'routing_key':'SendEmail'}}

