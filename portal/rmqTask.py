import pika
import sys
from rmqConnection import connection
import simplejson
from xml.dom.minidom import parse, parseString
import pdb
from rmqConfig import MAIN_EXCHANGE, QUEUE_MAP 
import socket
import re
import random
#from tasks import *

host_name = socket.getfqdn()

class Task:

    def __init__(self):
        self.connection = connection().get_connection()
        self.channel = self.connection.channel()
        self.queue = ''
        self.message = ''
        self.main_exchange = MAIN_EXCHANGE
        self.queue_map = QUEUE_MAP

    def get_logging(self, message):
        
        self.queue = 'logstash'
        self.channel.queue_declare(queue = self.queue, durable = True)
        self.message = message #' '.join(sys.argv[1:]) or "Hello World!"
        message_prop = pika.BasicProperties(
                         content_type = 'application/json',
                         delivery_mode = 2, # make message persistent
                      )
        self.channel.basic_publish(exchange = '',
                      routing_key = self.queue,
                      body = self.message,
                      properties = message_prop)
        #print " [x] Sent %r" % (self.message)
    
    def close_connection(self):
        self.connection.close()

    def publish_message(self, message, code):
        self.message = message
        self.queue = self.queue_map[code]['name']
        self.routing_key = self.queue_map[code]['routing_key']
        message_prop = pika.BasicProperties(
                         content_type = 'application/json',
                         delivery_mode = 2, # make message persistent
                      )
        self.channel.basic_publish(exchange = self.main_exchange['name'],
                      routing_key = self.routing_key,
                      body = self.message,
                      properties = message_prop)
        print " Published message - %s to %s" % (self.message, code)

    def consume_message(self, dest_exchange, queue_name, routing_key):

        result = self.channel.queue_declare(queue=queue_name, durable=True)
        queue_name = result.method.queue

        self.channel.queue_bind(exchange=dest_exchange, queue=queue_name, routing_key=routing_key)
        print ' [*] Waiting for logs. To exit press CTRL+C'

        def callback(ch, method, properties, body):
            print " [x] %r:%r" % (method.routing_key, body,)
            #resp = eval(queue_name)(simplejson.loads(body))
            print "%s - %s" % (queue_name,resp)
         
        self.channel.basic_consume(callback, queue=queue_name, no_ack=True)
        self.channel.start_consuming()
